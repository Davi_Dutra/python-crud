import sqlite3

# By: Davi Dutra

class Agenda:
    def __init__(self):
        # Conectando com o banco de dados
        self.conn = sqlite3.connect('agenda.db')
        # Criando o cursor
        self.cur = self.conn.cursor()
        # Criando a tabela agenda
        self.cur.execute("""CREATE TABLE IF NOT EXISTS agenda (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            nome VARCHAR(100),
            telefone VARCHAR(20),
            email VARCHAR(40),
            endereco TEXT)""")
    
    def insertContact(self, nome, telefone, email, endereco):
        self.cur.execute("""INSERT INTO agenda VALUES (null,?,?,?,?)""",
            (nome, telefone, email, endereco))
        self.conn.commit()
    
    def consultAllContacts(self):
        self.cur.execute("SELECT nome from agenda")
        return self.cur.fetchall()
    
    def consultContact(self, nome):
        self.cur.execute("SELECT * FROM agenda WHERE nome='{}'".format(nome))
        return self.cur.fetchall()
    
    def findId(self, nome):
        self.cur.execute("SELECT id FROM agenda WHERE nome='{}'".format(nome))
        return self.cur.fetchall()

    def updateContact(self, nome, telefone, email, endereco, id_):
        self.cur.execute("""UPDATE agenda SET 
            nome='{}',telefone='{}',email='{}',endereco='{}' WHERE id='{}'""".format(
                nome, telefone, email, endereco, id_))
        self.conn.commit()
    
    def deleteContact(self, id_):
        self.cur.execute("DELETE FROM agenda WHERE id='{}'".format(id_))
        self.conn.commit()
    
    def deleteAll(self):
        self.cur.execute("DELETE FROM agenda")
        self.conn.commit()
    
    def close(self):
        self.cur.close()
        self.conn.close()

if __name__ == '__main__':
    agenda = Agenda()