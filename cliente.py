from banco import Agenda
from time import sleep

agenda = Agenda()

print('\033[34m') # Cor da letra
print("""\
 _______________________________
|      Agenda de Contatos       |
|-------------------------------|
| Funções                       |
|                               |
| 1 - Adicionar contato         |
| 2 - Consultar contato         |
| 3 - Atualizar contato         |
| 4 - Listar todos os contatos  |
| 5 - Remover contato           |
| 6 - Sair                      |
|_______________________________|
""")
while True:
    try:
        opcao = int(input("Digite a opção desejada: "))
        if opcao == 1:
            print("\nPreencha os dados do contato: ")
            nome = input("Nome: ")
            telefone = input("Telefone: ")
            email = input("Email: ")
            endereco = input("Endereço: ")
            agenda.insertContact(nome, telefone, email, endereco)
            print("Contato cadastrado com sucesso!\n")

        elif opcao == 2:
            nome = input("\nDigite o nome do contato: ")
            contato = agenda.consultContact(nome)
            if contato:
                print("\nNome: {}\nTelefone: {}\nEmail: {}\nEndereço: {}\n".format(
                    contato[0][1], contato[0][2], contato[0][3], contato[0][4]))
            else:
                print("Contato não encontrado\n")

        elif opcao == 3:
            nome = input("\nDigite o nome do contato: ")
            id_ = agenda.findId(nome)
            if id_:
                print("\nPreencha os novos dados do contato: ")
                nome = input("Nome: ")
                telefone = input("Telefone: ")
                email = input("Email: ")
                endereco = input("Endereço: ")
                agenda.updateContact(nome, telefone, email, endereco, id_[0][0])
                print("Contato cadastrado com sucesso!\n")
            else:
                print("Contato não encontrado\n")

        elif opcao == 4:
            contatos = agenda.consultAllContacts()
            if contatos:
                for contato in contatos:
                    print("\nNome: {}".format(contato[0]), end="")
            else:
                print("Agenda vazia")
            print('\n')

        elif opcao == 5:
            nome = input("\nDigite o nome do contato: ")
            id_ = agenda.findId(nome)
            if id_:
                sn = input("\nDeseja excluir o contato de {}? sim/NÃO ".format(nome))
                if sn.upper() == "SIM":
                    agenda.deleteContact(id_[0][0])
                    print("Contato excluido com sucesso\n")
                else:
                    print("Cancelado\n")
            else:
                print("Contato não encontrado\n")

        elif opcao == 6:
            agenda.close()
            print("\nFinalizando..")
            sleep(3)
            print()
            quit()
        else:
            print("Opção Inválida\nTente novamente\n")

    except ValueError:
        print("Digite apenas números\n")